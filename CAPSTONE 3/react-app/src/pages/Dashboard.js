import { useState, useEffect} from "react"
import DashboardTemplate from "../components/DashboardTemplate"


export default function DashBoard(){
	const [AllProducts, setAllProducts] = useState([]);
	

	// Retrieves the courses from database upon initial render of the "Courses" Component

	useEffect(() => {
		fetch("http://localhost:4002/product/all")
		.then(res => res.json())
		.then(data => {
			console.log(data);


			setAllProducts(data.map(product => {
				console.log(product)
				return (
					<DashboardTemplate key={product._id} dashProp={product}/>
					)
			}))

		})
	}, [])


	
	return(
			<>
				<h1>Products</h1> 
				{/*Prop making ang prop passing*/}
				{AllProducts}
			</>


		)

}

