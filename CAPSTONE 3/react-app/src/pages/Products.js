import ProductCard from "../components/ProductCard"
import { useState, useEffect, useContext } from "react"

export default function Products(){

	const [AllProducts, setAllProducts] = useState([]);
	// Retrieves the courses from database upon initial render of the "Courses" Component

	useEffect(() => {
		fetch("http://localhost:4002/product/active")
		.then(res => res.json())
		.then(data => {
			console.log(data);


			setAllProducts(data.map(product => {
				console.log(product)
				return (
					<ProductCard key={product._id} productProp={product}/>
					)
			}))

		})
	}, [])


	return(

			<>
				<h1>Products</h1>
				{/*Prop making ang prop passing*/}
				{AllProducts}
			</>


		)
}