import ProductCard from "../components/ProductCard"
import { useState, useEffect, useContext } from "react"

export default function Products(){

  const [AllOrder, setAllOrders] = useState([]);
  // Retrieves the courses from database upon initial render of the "Courses" Component

  useEffect(() => {
    fetch("http://localhost:4002/order/")
    .then(res => res.json())
    .then(data => {
      console.log(data);


      setAllOrders(data.map(order => {
        console.log(order)
        return (
          <ProductCard key={order} productProp={order}/>
          )
      }))

    })
  }, [])


  return(

      <>
        <h1>Products</h1>
        {/*Prop making ang prop passing*/}
        {AllOrder}
      </>


    )
}














/*
import { Link } from "react-router-dom"
import ProductCard from "../components/ProductCard"
import { useState, useEffect, useContext } from "react"
import { Button } from "react-bootstrap"
import UserContext from "../UserContext"

export default function PlaceOrder({orderProp}){

  const { _id, name, description, price, numberOfStock } = orderProp
	const [ orders, setOrders] = useState("kk")
	const { user } = useContext(UserContext)
	
	useEffect(() => {

		fetch(`http://localhost:4002/product/${_id}`, {
			method: "GET",
			header: {
				"Content-type" : "application/json"
			},
			body: JSON.stringify({
				
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})
	})
	return (
			<>
			<h1>Orders</h1>
			<Button >Checkout</Button>
			</>
		)
}
*/



/*import React, { useState } from 'react';

const OrderForm = () => {
  // State variables for storing form data
  const [products, setProducts] = useState([]);
  const [errorMessage, setErrorMessage] = useState('');

  // Handle form submission
  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      // Send a POST request to the backend API
      const response = await fetch('/api/orders', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'your-auth-token',
        },
        body: JSON.stringify({ products }),
      });

      if (response.ok) {
        const order = await response.json();
        console.log('Order created:', order);
        // Reset the form and display a success message
        setProducts([]);
        setErrorMessage('');
      } else {
        const error = await response.json();
        throw new Error(error.message);
      }
    } catch (error) {
      console.error('Error creating order:', error);
      setErrorMessage(error.message);
    }
  };

  // Handle changes to the form fields
  const handleProductChange = (e, index) => {
    const { name, value } = e.target;
    const newProducts = [...products];
    newProducts[index] = { ...newProducts[index], [name]: value };
    setProducts(newProducts);
  };

  // Add a new product input field
  const handleAddProduct = () => {
    setProducts([...products, { productId: '', quantity: 1 }]);
  };

  // Remove a product input field
  const handleRemoveProduct = (index) => {
    const newProducts = [...products];
    newProducts.splice(index, 1);
    setProducts(newProducts);
  };

  return (
    <div>
      <h2>Order Form</h2>
      <form onSubmit={handleSubmit}>
        {products.map((product, index) => (
          <div key={index}>
            <label>
              Product ID:
              <input
                type="text"
                name="productId"
                value={product.productId}
                onChange={(e) => handleProductChange(e, index)}
              />
            </label>
            <label>
              Quantity:
              <input
                type="number"
                name="quantity"
                value={product.quantity}
                onChange={(e) => handleProductChange(e, index)}
              />
            </label>
            <button type="button" onClick={() => handleRemoveProduct(index)}>
              Remove
            </button>
          </div>
        ))}
        <button type="button" onClick={handleAddProduct}>
          Add Product
        </button>
        <button type="submit">Submit Order</button>
      </form>
      {errorMessage && <p>{errorMessage}</p>}
    </div>
  );
};

export default OrderForm;*/
