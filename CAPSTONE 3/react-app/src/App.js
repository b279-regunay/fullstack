// destructured tools
import {Container} from "react-bootstrap"
import { Route, Routes } from "react-router-dom"
import { BrowserRouter as Router } from "react-router-dom"
import { UserProvider } from "./UserContext"
import { useState, useEffect } from "react"

// Pages
import AppNavbar from "./components/AppNavbar"
import './App.css';
import Home from "./pages/Home"
import Products from "./pages/Products"
import Register from "./pages/Register"
import Login from "./pages/Login"
import Dashboard from "./pages/Dashboard"
import Order from "./pages/Order"
import Logout from "./pages/Logout"

function App(){

  const [user, setUser] = useState({
      id: null,
      isAdmin: null,
      email: null,
      token: localStorage.getItem("token")
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (

      <>
      <UserProvider value={{user, setUser, unsetUser}}>
      <Router>  
        <AppNavbar/>
          <Container>
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/product" element={<Products/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path="/dashboard" element={<Dashboard/>}/>   
              <Route path="/order" element={<Order/>}/>   
              <Route path="/logout" element={<Logout/>}/>
            </Routes>
          </Container>
        </Router>
      </ UserProvider>
      </>

    )
}

export default App;