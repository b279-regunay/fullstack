import React, { useState, useContext } from 'react';
import { Button, Card, Modal, Form } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductCard({ productProp }) {
  const { user } = useContext(UserContext);
  const { _id, name, description, price, numberOfStock } = productProp;
  const [showModal, setShowModal] = useState(false);
  const [quantity, setQuantity] = useState(1);

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleShowModal = () => {
    setShowModal(true);
  };

  const buyNow = () => {
    fetch('http://localhost:4002/order/orders', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${user.token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        products: [
          {
            productId: _id,
            quantity: quantity,
          },
        ],
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        Swal.fire({
          icon: 'success',
          title: 'Order Received!',
          text: 'Thank you for your purchase!',
        });
      });
  };

  const handleQuantityChange = (event) => {
    const value = parseInt(event.target.value, 10);

    if (value >= 1 && value <= 100) {
      setQuantity(value);
    }
  };

  return (
    <Card style={{ width: '18rem' }}>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>{description}</Card.Text>
        <Card.Text>Price: {price}</Card.Text>
        <Card.Text>{numberOfStock}</Card.Text>
      </Card.Body>
      {!user.isAdmin && (
        <Button onClick={handleShowModal}>Place Order</Button>
      )}
      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Update Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group>
            <Form.Label>Name</Form.Label>
            <Form.Control type="text" value={name} />
          </Form.Group>

          <Form.Group>
            <Form.Label>Description</Form.Label>
            <Form.Control type="text" value={description} />
          </Form.Group>

          <Form.Group>
            <Form.Label>Price</Form.Label>
            <Form.Control type="text" value={price} />
          </Form.Group>
          <Form.Group>
            <Form.Label>Quantity</Form.Label>
            <Form.Control
              type="number"
              min="1"
              max="100"
              value={quantity}
              onChange={handleQuantityChange}
            />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Close
          </Button>
          <Button variant="secondary" onClick={buyNow}>
            Buy Now
          </Button>
          <Button variant="primary">Add to Cart</Button>
        </Modal.Footer>
      </Modal>
    </Card>
  );
}















// import Button from 'react-bootstrap/Button';
// import Card from 'react-bootstrap/Card';
// import { Link } from "react-router-dom"
// import { Modal, Form } from 'react-bootstrap';
// import { useState } from "react"
 
// export default function productCard({productProp}){

// 	const { _id, /*imageSrc,*/ name, description, price, numberOfStock  } = productProp;
// 	const [showModal, setShowModal] = useState(false);

// 	const orderProduct = () => {
// 		fetch(`http://localhost:4002/product/${_id}`, {
// 			method: "GET",
// 			header: {
// 				"Content-type" : "application/json"
// 			},
// 		})
// 		.then(res => res.json())
// 		.then(data => {
// 			console.log(data)
// 			console.log(data.name)
// 			handleCloseModal()
// 		})
// 	}

// 	const handleCloseModal = () => {
// 	  setShowModal(false);
// 	};

// 	const handleShowModal = () => {
// 	  setShowModal(true);
// 	};


// 	return (
// 		<>
// 			<Card style={{ width: '18rem' }}>
// {/*			     <Card.Img variant="top" src={imageSrc} />*/}
// 			     <Card.Body>
// 			       <Card.Title>{name}</Card.Title>
// 			       <Card.Text>
// 			         {description}
// 			       </Card.Text>
// 			       <Card.Text>
// 			         Price: {price}
// 			       </Card.Text>
// 			       <Card.Text>
// 			         {numberOfStock}
// 			       </Card.Text>
// 			     </Card.Body>
// 			     <Button onClick={handleShowModal}>Place Order</Button>
// 			   </Card>



// 			<Modal show={showModal} onHide={handleCloseModal}>
// 			  <Modal.Header closeButton>
// 			    <Modal.Title>Update Product</Modal.Title>
// 			  </Modal.Header>
// 			  <Modal.Body>
// 			    <Form.Group>
// 			      <Form.Label>Name</Form.Label>
// 			      <Form.Control
// 			        type="text"
// 			        value={name}
			        
// 			      />
// 			    </Form.Group>

// 			    <Form.Group>
// 			      <Form.Label>Description</Form.Label>
// 			      <Form.Control
// 			        type="text"
// 			        value={description}
			  
// 			      />
// 			    </Form.Group>

// 			    <Form.Group>
// 			      <Form.Label>Price</Form.Label>
// 			      <Form.Control
// 			        type="text"
// 			        value={price}
// 			      />
// 			    </Form.Group>
// 			    <Form.Group>
// 			      <Form.Label>Number of Stock</Form.Label>
// 			      <Form.Control
// 			        type="text"
// 			        value={numberOfStock}
// 			      />
// 			    </Form.Group>
// 			  </Modal.Body>
// 			  <Modal.Footer>
// 			    <Button variant="secondary" onClick={handleCloseModal}>
// 			      Close
// 			    </Button>
// 			    <Button variant="primary">
// 			      Save Changes
// 			    </Button>
// 			  </Modal.Footer>
// 			</Modal>

// 		</>

// 		)


// }

