import { Nav, Navbar, Container, NavLink } from "react-bootstrap"
import { Link } from "react-router-dom"
import Login from "../pages/Login"
import UserContext from "../UserContext";
import { useContext } from "react"

export default function AppNavbar(){

  const { user } = useContext(UserContext);

	return (
    <>
      {
        (user.isAdmin ) ?

          <Navbar bg="light" expand="lg" className="px-3">
              <Navbar.Brand as={Link} to={"/"} className="fw-bold">Zuitt</Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ms-auto">
                  <Nav.Link  as={Link} to={"/"}>Home</Nav.Link>
                  <Nav.Link as={Link} to="/product">Products</Nav.Link>

                  <Nav.Link as={Link} to={"/dashboard"}>Dashboard</Nav.Link>
                </Nav>
              </Navbar.Collapse>
          </Navbar>

          :

        <Navbar bg="light" expand="lg" className="px-3">
            <Navbar.Brand as={Link} to={"/"} className="fw-bold">Zuitt</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ms-auto">
                <Nav.Link  as={Link} to={"/"}>Home</Nav.Link>
                <Nav.Link as={Link} to="/product">Products</Nav.Link>
                <Nav.Link as={Link} to={"/order"}>Order</Nav.Link>
                {
                  (user.token !== null) ?
                  <Nav.Link as={Link} to={"/logout"}>Logout</Nav.Link>

                  :
                  <>
                  <Nav.Link as={Link} to={"/register"}>Register</Nav.Link>
                  <Nav.Link as={Link} to={"/login"}>Login</Nav.Link>
                  </>
                }
                
              </Nav>
            </Navbar.Collapse>
        </Navbar>

      }
      </>
		)
}
