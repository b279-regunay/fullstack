import { Row, Col, Button} from "react-bootstrap"
import { Link } from "react-router-dom"

export default function Banner({bannerProps}) {

	const {title, content, destination, label} = bannerProps

	return (

			<Row>
				<Col>
					<h1>{title}</h1>
					<p>{content}</p>
					<Button as={Link} to={destination}>{label}</Button>
				</Col>
			</Row>

	)
}