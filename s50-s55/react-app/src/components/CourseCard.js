import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
// In React.js we have 3 hooks
/*
1. useState
2. useEffect
3. useContext
*/

import { useState } from "react"
import {Link} from "react-router-dom"

export default function CourseCard({courseProp}){


	// Destructuring courseProp into their own variables
	const { _id, name, description, price } = courseProp;

/*	// useState
	// Used for storing different states
	// Used to keep track of information to individual components
	
	// SYNTAX -> const [getter, setter] = useState(initialGetterValue);

	const [count, setCount] = useState(0);
	console.log(useState(0));

	const [seat, setSeat] = useState(30);

	// Function that keeps track of the enrollees for a course
	function enroll(){
		setCount(count + 1);
		console.log("Enrollees: " + count );

		setSeat(seat - 1)
		if(seat === 0){
			alert("No more seats.")
		}
	}*/

	return (
			<Card style={{ width: '18rem' }}>
			      <Card.Body>
			        <Card.Title>{name}</Card.Title>
			        <Card.Text>
			         {description}
			        </Card.Text>
			        <Card.Text>
			         Price:
			        </Card.Text>
			        <Card.Text>
			         {price}
			        </Card.Text>
{/*			        <Card.Subtitle>
			         Enrollees
			        </Card.Subtitle>
			        <Card.Text>
			         {count}
			        </Card.Text>*/}
{/*			        <Button variant="primary" onClick={enroll}>Enroll</Button>*/}
			        <Link className="btn btn-primary" to={`/CourseView/${_id}`}>Details</Link>
			      </Card.Body>
			    </Card>

		)
}