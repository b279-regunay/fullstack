import courseData from "../data/course"
import CourseCard from "../components/CourseCard"
import {useEffect, useState } from "react"


export default function Courses(){
	console.log(courseData);
	console.log(courseData[0]);

	// State that will be used to store courses retrieved from db
	const [allCourses, setAllCourses] = useState([]);



	// Retrieves the courses from database upon initial render of the courses components
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);


			setAllCourses(data.map(course => {
				return (<CourseCard  key={course._id} courseProp={course}/>
					)
			}))


		})
	}, [])


	return(
		<>
			<h1>Courses</h1>
			{allCourses}
		</>
		)
}