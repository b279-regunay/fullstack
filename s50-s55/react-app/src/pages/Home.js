import Banner from "../components/Banner"
import Highlights from "../components/Highlights"

export default function Home(){
	const data = {
		title: "Zuitt Coding Bootcamp",
		content: "Duties for everyone, for er",
		destination: "/courses",
		label: "Enroll Now"
	}


	return(
			<>
				<Banner bannerProps={data}/>
				<Highlights/>
			</>
		)
}